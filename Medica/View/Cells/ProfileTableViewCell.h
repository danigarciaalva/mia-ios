//
//  ProfileTableViewCell.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MemberTableViewCell.h"

@interface ProfileTableViewCell : MemberTableViewCell
- (IBAction)onSubscribeClick:(id)sender;

@end
