//
//  MemberTableViewCell.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Member.h"

@protocol MemberCellDelegate <NSObject>

-(void)onSubscribeMember:(Member*)member;

@end

typedef NS_ENUM(NSInteger, MemberSubscriptionStyle) {
    MemberSubscriptionStyleSubscribe,
    MemberSubscriptionStyleCancel
};

@interface MemberTableViewCell : UITableViewCell
@property (weak) id<MemberCellDelegate> delegate;
@property (nonatomic, strong) Member* member;
@property (weak, nonatomic) IBOutlet UILabel *memberNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *memberRelationshipLabel;
@property (weak, nonatomic) IBOutlet UIButton *subscriptionActionButton;
@property (weak, nonatomic) IBOutlet UIImageView *memberImageView;

-(void)setSubscriptionStyle:(MemberSubscriptionStyle)style;
@end
