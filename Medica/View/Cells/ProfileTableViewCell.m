//
//  ProfileTableViewCell.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "ProfileTableViewCell.h"

@implementation ProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.subscriptionActionButton.layer.borderWidth = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setSubscriptionStyle:(MemberSubscriptionStyle)style{
    if (style == MemberSubscriptionStyleSubscribe) {
        [self.subscriptionActionButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [self.subscriptionActionButton setTitle:NSLocalizedString(@"Text_Subscribe", nil) forState:UIControlStateNormal];
    } else {
        [self.subscriptionActionButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.subscriptionActionButton setTitle:NSLocalizedString(@"Text_Subscribed", nil) forState:UIControlStateNormal];
        [self.subscriptionActionButton setEnabled:false];
    }
}

- (IBAction)onSubscribeClick:(id)sender {
}
@end
