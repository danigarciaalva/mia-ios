//
//  MemberTableViewCell.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "MemberTableViewCell.h"

@implementation MemberTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void)layoutSubviews{
    self.subscriptionActionButton.layer.cornerRadius = 4;
    self.subscriptionActionButton.layer.borderWidth = 1.0;
    self.memberImageView.layer.cornerRadius = self.memberImageView.frame.size.width / 2;
    self.memberImageView.layer.masksToBounds = true;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onSubscriptionActionClick:(id)sender {
    [self.delegate onSubscribeMember:self.member];
}

-(void)setSubscriptionStyle:(MemberSubscriptionStyle)style{
    if (style == MemberSubscriptionStyleSubscribe) {
        self.subscriptionActionButton.layer.borderColor = [UIColor blueColor].CGColor;
        [self.subscriptionActionButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [self.subscriptionActionButton setTitle:NSLocalizedString(@"Title_Button_Subscribe", nil) forState:UIControlStateNormal];
    } else {
        self.subscriptionActionButton.layer.borderColor = [UIColor whiteColor].CGColor;
        [self.subscriptionActionButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.subscriptionActionButton setTitle:NSLocalizedString(@"Text_Subscribed", nil) forState:UIControlStateNormal];
        [self.subscriptionActionButton setEnabled:false];
    }
}

@end
