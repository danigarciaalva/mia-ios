//
//  SettingsDatasource.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "SettingsDatasource.h"
#import "ProfileTableViewCell.h"
#import "MemberTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AddMemberTableViewCell.h"

#define SECTION_PROFILE 0
#define SECTION_ADDITIONALS 1

@implementation SettingsDatasource{
    NSArray<Member*> * _additionals;
    Member * _anchorMember;
    UITableView * _tableView;
    SEL onAnchorSuscribeClick;
}

-(id)initWithTableView:(UITableView*)tableView andAnchorSuscribeClick:(SEL)suscribeClick{
    self = [super init];
    if (self) {
        _tableView = tableView;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        onAnchorSuscribeClick = suscribeClick;
    }
    return self;
}

-(void)updateAnchor:(Member *)anchor andAdditionals:(NSArray<Member *> *)additionals{
    _anchorMember = anchor;
    _additionals = additionals;
    [_tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case SECTION_PROFILE: return 1;
        case SECTION_ADDITIONALS: return _additionals.count + 1;
        default: return 0;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * profileIdentifier = @"ProfileTableViewCell";
    static NSString * memberIdentifier = @"MemberTableViewCell";
    static NSString * addMemberIdentifier = @"AddMemberTableViewCell";
    
    switch (indexPath.section) {
        case SECTION_PROFILE:{
            ProfileTableViewCell * profileCell = [tableView dequeueReusableCellWithIdentifier:profileIdentifier];
            profileCell.memberNameLabel.text = _anchorMember.name;
            [profileCell setSubscriptionStyle: _anchorMember.subscription != nil && _anchorMember.subscription.isActive ? MemberSubscriptionStyleCancel : MemberSubscriptionStyleSubscribe];
            [profileCell.memberImageView sd_setImageWithURL:[NSURL URLWithString:_anchorMember.image]];
            [profileCell.subscriptionActionButton addTarget:self.controller action:onAnchorSuscribeClick forControlEvents:UIControlEventTouchUpInside];
            return profileCell;
        }
        case SECTION_ADDITIONALS:{
            if (indexPath.row == _additionals.count) {
                AddMemberTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:addMemberIdentifier];
                return cell;
            } else {
                Member * additional = _additionals[indexPath.row];
                MemberTableViewCell * memberCell = [tableView dequeueReusableCellWithIdentifier:memberIdentifier];
                memberCell.memberNameLabel.text = additional.name;
                memberCell.memberRelationshipLabel.text = additional.relationship;
                [memberCell.memberImageView sd_setImageWithURL:[NSURL URLWithString:additional.image]];
                [memberCell setSubscriptionStyle:additional.subscription != nil && additional.subscription.isActive ? MemberSubscriptionStyleCancel : MemberSubscriptionStyleSubscribe];
                memberCell.delegate = self;
                memberCell.member = additional;
                return memberCell;
            }
        }
        default: return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case SECTION_PROFILE:{
            return 110.0f;
        }
        case SECTION_ADDITIONALS:{
            if (indexPath.row != _additionals.count) {
                return 55.0f;
            } else {
                return 40.0f;
            }
        }
        default: return 44.0f;
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case SECTION_ADDITIONALS:{
            if (indexPath.row == _additionals.count) {
                [self.datasourceDelegate onAddMemberClick];
            }
        }
        default: break;
    }
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return section == 0 ? NSLocalizedString(@"Title_Members", nil) : NSLocalizedString(@"Title_Additionals", nil);
}

-(void)onSubscribeMember:(Member *)member{
    [self.datasourceDelegate onSubscriptionMemberActionClick:member];
}
@end
