//
//  SettingsDatasource.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Member.h"
#import "MemberTableViewCell.h"
#import "SettingsViewController.h"

@protocol SettingsDatasourceDelegate <NSObject>

-(void)onAddMemberClick;
-(void)onSubscriptionMemberActionClick:(Member*)member;

@end

@interface SettingsDatasource : NSObject<UITableViewDataSource, UITableViewDelegate, MemberCellDelegate>

@property (nonatomic, weak) id<SettingsDatasourceDelegate> datasourceDelegate;
@property (nonatomic, weak) SettingsViewController* controller;

-(void)updateAnchor:(Member*)anchor andAdditionals:(NSArray<Member*>*)additionals;
-(id)initWithTableView:(UITableView*)tableView andAnchorSuscribeClick:(SEL)suscribeClick;

@end
