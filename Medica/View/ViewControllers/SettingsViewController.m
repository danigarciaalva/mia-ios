//
//  SettingsViewController.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsPresenter.h"
#import "SettingsDatasource.h"
#import <STPopup/STPopup.h>
#import "AddMemberViewController.h"
#import "IAPHelper.h"

@interface SettingsViewController ()<SettingsDelegate, SettingsDatasourceDelegate, AddMemberDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SettingsViewController{
    SettingsPresenter * settingsPresenter;
    SettingsDatasource * settingsDatasource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    settingsPresenter = [[SettingsPresenter alloc] init];
    settingsPresenter.delegate = self;
    [self setupPresenter:settingsPresenter];
    
    settingsDatasource = [[SettingsDatasource alloc] initWithTableView:self.tableView andAnchorSuscribeClick:@selector(onSubscriptionActionClick:)];
    settingsDatasource.datasourceDelegate = self;
    settingsDatasource.controller = self;
    
    [settingsPresenter loadSubscriptions];
}

-(void)onSuccessLoadedAnchor:(Member *)anchor andAdditionals:(NSArray<Member *> *)additionals{
    [settingsDatasource updateAnchor:anchor andAdditionals:additionals];
}

-(void)onErrorLoadedMembers{
    
}

-(void)onSubscriptionActionClick:(UIButton*)button{
    [[IAPHelper sharedInstance] purchaseSubscription:MemberSubscriptionGeneral completitionHandler:^(BOOL success, NSString *error, NSString *transactionIdentifier, BOOL subscriptionActive) {
        if (success) {
            [settingsPresenter purchaseSubscriptionInAnchor:transactionIdentifier];
            [settingsPresenter loadSubscriptions];
        } else {
            NSLog(@"%@", error);
        }
    }];
}

-(void)onSubscriptionMemberActionClick:(Member *)member{
    [[IAPHelper sharedInstance] purchaseSubscription:MemberSubscriptionAdditional completitionHandler:^(BOOL success, NSString *error, NSString *transactionIdentifier, BOOL subscriptionActive) {
        if (success) {
            [settingsPresenter purchaseSubscriptionWithMember:member andTransactionIdentifier: transactionIdentifier];
            [settingsPresenter loadSubscriptions];
            NSLog(@"Transaction: %@ is active", transactionIdentifier);
        } else {
            NSLog(@"%@", error);
        }
    }];
}

-(void)onAddMemberClick{
    AddMemberViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AddMemberViewController"];
    controller.addMemberDelegate = self;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController: controller];
    popupController.containerView.layer.cornerRadius = 4;
    [popupController presentInViewController:self];
}

-(void)onSuccessSavedMember{
    [settingsPresenter loadSubscriptions];
}
@end
