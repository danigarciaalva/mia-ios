//
//  AddMemberViewController.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/15/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "AddMemberViewController.h"
#import <STPopup/STPopup.h>
#import "AddMemberPresenter.h"

@implementation AddMemberViewController{
    AddMemberPresenter * addMemberPresenter;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.title = @"Agregar miembro";
    self.contentSizeInPopup = CGSizeMake(300, 270);
    self.landscapeContentSizeInPopup = CGSizeMake(200, 270);
    self.view.layer.cornerRadius = 4;
    
    addMemberPresenter = [[AddMemberPresenter alloc] init];
    [self setupPresenter:addMemberPresenter];
}

- (IBAction)onAddClick:(id)sender {
    [addMemberPresenter saveMemberWithName:self.nameTextField.text andRelationship:self.relationshipTextField.text];
    [self dismissViewControllerAnimated:true completion:^{
        [self.addMemberDelegate onSuccessSavedMember];
    }];
}
@end
