//
//  ViewController.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePresenter.h"

@interface BaseViewController : UIViewController

@property (nonatomic, retain) BasePresenter * presenter;
-(void)setupPresenter:(BasePresenter*)presenter;
@end

