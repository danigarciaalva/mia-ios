//
//  AddMemberViewController.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/15/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "BaseViewController.h"
#import <JVFloatLabeledTextField/JVFloatLabeledTextField.h>

@protocol AddMemberDelegate;

@interface AddMemberViewController : BaseViewController
@property (retain) id<AddMemberDelegate> addMemberDelegate;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *nameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *relationshipTextField;

- (IBAction)onAddClick:(id)sender;
@end


@protocol AddMemberDelegate <NSObject>

-(void)onSuccessSavedMember;

@end