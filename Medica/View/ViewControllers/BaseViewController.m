//
//  ViewController.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.presenter) {
        [self.presenter loadPresenter];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.presenter) {
        [self.presenter unloadPresenter];
    }
}

-(void)setupPresenter:(BasePresenter *)presenter{
    self.presenter = presenter;
}
@end
