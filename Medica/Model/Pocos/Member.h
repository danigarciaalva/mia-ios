//
//  Member.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Subscription.h"
#import <Realm/Realm.h>

@interface Member : RLMObject

@property NSString * uuid;
@property Subscription * subscription;
@property NSString* name;
@property NSString* image;
@property NSString* relationship;
@property BOOL isAnchor;
@end
