//
//  Subscription.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Subscription : RLMObject

@property NSString* uuid;
@property NSString* initialDate;
@property BOOL isActive;
@property BOOL isEditable;
@property double amount;

@end
