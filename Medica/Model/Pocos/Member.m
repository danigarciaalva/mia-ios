//
//  Member.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "Member.h"

@implementation Member

-(id)init{
    self = [super init];
    if(self){
        self.uuid = [[NSUUID UUID] UUIDString];
    }
    return self;
}

+(NSString*)primaryKey{
    return @"uuid";
}

@end
