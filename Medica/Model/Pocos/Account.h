//
//  Profile.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Member.h"

@interface Account : NSObject

@property(nonatomic, strong) NSArray<Member*> * suscriptionMembers;

@end
