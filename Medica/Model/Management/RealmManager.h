//
//  RealmManager.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/15/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Member.h"

@interface RealmManager : NSObject

+(Member*)findAnchor;
+(void)insertMember:(Member*)member;
+(NSMutableArray<Member*>*)loadMembers;
@end
