//
//  RealmManager.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/15/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "RealmManager.h"

@implementation RealmManager

+(Member*)findAnchor{
    return [[Member objectsWhere:@"isAnchor = true"] firstObject];
}

+(void)insertMember:(Member *)member{
    RLMRealm * realm = [RLMRealm defaultRealm];
    [realm transactionWithBlock:^{
        [Member createInRealm:realm withValue:member];
    }];
}

+(NSMutableArray<Member*>*)loadMembers{
    RLMResults * results = [Member objectsWhere:@"isAnchor = false"];
    NSMutableArray * members = [[NSMutableArray alloc] init];
    for(Member * member in results){
        [members addObject:member];
    }
    return members;
}
@end
