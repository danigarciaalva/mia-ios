//
//  IAPHelper.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/14/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import <StoreKit/StoreKit.h>

UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;

#define kSubscriptionGeneral @"dflabs_medica_subscription_general"
#define kSubscriptionAdditional @"dflabs_medica_subscription_additional"

typedef void (^SubscriptionCompletionHandler)(BOOL success, NSString* error, NSString* transactionIdentifier, BOOL subscriptionActive);

typedef NS_ENUM(NSInteger, MemberSubscriptionType) {
    MemberSubscriptionGeneral,
    MemberSubscriptionAdditional
};

@interface IAPHelper : NSObject

+ (IAPHelper *)sharedInstance;
- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;

-(void)purchaseSubscription:(MemberSubscriptionType)type completitionHandler:(SubscriptionCompletionHandler)completionHandler;
- (void)restoreCompletedTransactions;

-(int)daysRemainingOnSubscription:(NSDate*)expiryDate;
-(NSString *)getExpiryDateString:(NSDate*)expiryDate;

@end

