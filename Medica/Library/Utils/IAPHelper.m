//
//  IAPHelper.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/14/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "IAPHelper.h"
#import <StoreKit/StoreKit.h>
#import "VerificationController.h"

NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";

@interface IAPHelper () <SKProductsRequestDelegate, SKPaymentTransactionObserver>
@end

@implementation IAPHelper {
    SKProductsRequest * _productsRequest;
    NSSet * _productIdentifiers;
    NSArray * _availableProducts;
    SubscriptionCompletionHandler _completitionHandler;
}

+(IAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static IAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      kSubscriptionGeneral,
                                      kSubscriptionAdditional,
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}


- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers{
    
    if ((self = [super init])) {
        // Store product identifiers
        _productIdentifiers = productIdentifiers;
        // Add self as transaction observer
        _availableProducts = [[NSArray alloc] init];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
        _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
        _productsRequest.delegate = self;
        [_productsRequest start];
    }
    return self;
    
}

-(void)purchaseSubscription:(MemberSubscriptionType)type completitionHandler:(SubscriptionCompletionHandler)completionHandler {
    _completitionHandler = [completionHandler copy];
    
    NSString * _type = type == MemberSubscriptionGeneral ? kSubscriptionGeneral : kSubscriptionAdditional;
    SKProduct * _product = nil;
    for (SKProduct * product in _availableProducts) {
        if ([product.productIdentifier isEqualToString:_type]) {
            _product = product;
            break;
        }
    }
    if (_product != nil) {
        [self buyProduct:_product];
    } else {
        if (_completitionHandler != nil) {
            _completitionHandler(false, @"Product not available", nil, false);
        }
    }
}

- (void)buyProduct:(SKProduct *)product {
    NSLog(@"Buying %@...", product.productIdentifier);
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

- (void)validateReceiptForTransaction:(SKPaymentTransaction *)transaction {
    VerificationController * verifier = [VerificationController sharedInstance];
    [verifier verifyPurchase:transaction completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Successfully verified receipt!");
            if (_completitionHandler != nil) {
                _completitionHandler(true, nil, transaction.transactionIdentifier, true);
            }
        } else {
            if (_completitionHandler != nil) {
                _completitionHandler(false, @"Failed to verify receipt", transaction.transactionIdentifier,
                                 false);
            }
            [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
        }
    }];
}

#pragma mark - Utilities
-(int)daysRemainingOnSubscription:(NSDate*)expiryDate {
    NSDateFormatter *dateformatter = [NSDateFormatter new];
    [dateformatter setDateFormat:@"dd MM yyyy"];
    NSTimeInterval timeInt = [[dateformatter dateFromString:[dateformatter stringFromDate:expiryDate]] timeIntervalSinceDate: [dateformatter dateFromString:[dateformatter stringFromDate:[NSDate date]]]]; //Is this too complex and messy?
    int days = timeInt / 60 / 60 / 24;
    
    if (days >= 0) {
        return days;
    } else {
        return 0;
    }
}

-(NSString *)getExpiryDateString:(NSDate*)date {
    if ([self daysRemainingOnSubscription:date] > 0) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        return [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:date]];
    } else {
        return @"";
    }
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {    
    NSLog(@"Loaded list of products...");
    _productsRequest = nil;
    _availableProducts = response.products;
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;
    _availableProducts = [[NSArray alloc] init];
    
}

#pragma mark SKPaymentTransactionOBserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    [self validateReceiptForTransaction:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");
    [self validateReceiptForTransaction:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"failedTransaction...");
    if (transaction.error.code != SKErrorPaymentCancelled){
        NSString * error = [NSString stringWithFormat:@"Transaction error: %@", transaction.error.localizedDescription];
        if (_completitionHandler != nil) {
            _completitionHandler(false, error, transaction.transactionIdentifier, false);
        }
    } else {
        if (_completitionHandler != nil) {
            _completitionHandler(false, @"Transaction cancelled", transaction.transactionIdentifier, false);
        }
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)restoreCompletedTransactions {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

@end
