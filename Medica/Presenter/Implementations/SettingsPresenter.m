//
//  SettingsPresenter.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "SettingsPresenter.h"
#import "Member.h"
#import "Subscription.h"
#import "RealmManager.h"

@implementation SettingsPresenter

-(void)loadSubscriptions{
    
    Member * anchor = [RealmManager findAnchor];
    
    NSMutableArray<Member*>* additionals = [RealmManager loadMembers];
    if (self.delegate && [self.delegate respondsToSelector:@selector(onSuccessLoadedAnchor:andAdditionals:)]) {
        [self.delegate onSuccessLoadedAnchor:anchor andAdditionals:additionals];
    }
}

-(void)purchaseSubscriptionInAnchor:(NSString *)transactionIdentifier {
    Member * anchor = [RealmManager findAnchor];
    [self purchaseSubscriptionWithMember:anchor andTransactionIdentifier:transactionIdentifier];
}

-(void)purchaseSubscriptionWithMember:(Member *)member andTransactionIdentifier:(NSString *)transactionIdentifier{
    RLMRealm * realm = [RLMRealm defaultRealm];
    [realm transactionWithBlock:^{
        Subscription * subscription;
        if (member.subscription == nil) {
            subscription = [[Subscription alloc] init];
        }
        subscription.isActive = true;
        subscription.isEditable = !member.isAnchor;
        subscription.amount = member.isAnchor ? 35 : 18;
        if (member.subscription == nil) {
            member.subscription = subscription;
            [Member createOrUpdateInRealm:realm withValue:member];
        } else {
            [Subscription createOrUpdateInRealm:realm withValue:subscription];
        }
    }];
}

@end
