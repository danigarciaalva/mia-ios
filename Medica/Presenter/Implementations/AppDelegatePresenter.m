//
//  AppDelegatePresenter.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/15/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "AppDelegatePresenter.h"
#import "RealmManager.h"

@implementation AppDelegatePresenter

-(void)setup {
    Member* anchor = [RealmManager findAnchor];
    if (anchor == nil){
        anchor = [[Member alloc] init];
        anchor.name = @"Daniel García Alvarado";
        anchor.image = @"http://www.morganstanley.com/pub/content/dam/msdotcom/people/tiles/michael-asmar.jpg.img.490.medium.jpg/1442516280558.jpg";
        anchor.isAnchor = true;
        [RealmManager insertMember: anchor];
    }
}
@end
