//
//  AddMemberPresenter.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/15/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "BasePresenter.h"

@interface AddMemberPresenter : BasePresenter

-(void)saveMemberWithName:(NSString*)name andRelationship:(NSString*)relationship;
@end
