//
//  AddMemberPresenter.m
//  Medica
//
//  Created by Daniel García Alvarado on 3/15/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "AddMemberPresenter.h"
#import "Member.h"
#import "RealmManager.h"

@implementation AddMemberPresenter

-(void)saveMemberWithName:(NSString *)name andRelationship:(NSString *)relationship{
    Member * member = [[Member alloc] init];
    member.name = name;
    member.relationship = relationship;
    member.isAnchor = false;
    member.image = @"http://www.morganstanley.com/pub/content/dam/msdotcom/people/tiles/michael-asmar.jpg.img.490.medium.jpg/1442516280558.jpg";
    [RealmManager insertMember:member];
}
@end
