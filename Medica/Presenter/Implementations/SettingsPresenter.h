//
//  SettingsPresenter.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/13/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import "BasePresenter.h"
#import "Member.h"

@protocol SettingsDelegate;

@interface SettingsPresenter : BasePresenter
@property (nonatomic, assign) id<SettingsDelegate> delegate;

-(void)loadSubscriptions;
-(void)purchaseSubscriptionInAnchor:(NSString *)transactionIdentifier;
-(void)purchaseSubscriptionWithMember:(Member*)member andTransactionIdentifier:(NSString*)transactionIdentifier;
@end


@protocol SettingsDelegate <NSObject>

-(void)onSuccessLoadedAnchor:(Member*)anchor andAdditionals:(NSArray<Member*>*) additionals;
-(void)onErrorLoadedMembers;

@end