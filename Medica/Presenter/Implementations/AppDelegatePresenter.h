//
//  AppDelegatePresenter.h
//  Medica
//
//  Created by Daniel García Alvarado on 3/15/16.
//  Copyright © 2016 Dragonfly Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppDelegatePresenter : NSObject

-(void)setup;
@end
